import multiprocessing
import os

# tuple met alle processen
all_processes = ('NS_personeel_paneel.py','NS_invoer_zuil.py')


def execute(process):
    'Execute elk proces in de tuple'
    os.system(f'python {process}')


if __name__ == '__main__':
    process_pool = multiprocessing.Pool(processes=2)
    process_pool.map(execute, all_processes)