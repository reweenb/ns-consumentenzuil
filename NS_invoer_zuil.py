from tkinter import *

def clicked():
    '''Opent een text file en schrijft hierin de input van het entry veld weg'''
    outfile = open('message.txt', 'w')
    text = entry.get()
    outfile.write(text)
    outfile.close()

root = Tk()
#root.overrideredirect(True)


#fullscreen
root.geometry("{0}x{1}+0+0".format(root.winfo_screenwidth(), root.winfo_screenheight()))


#labels
label = Label(master=root, text='Welkom bij de NS Consumentenzuil, deel op twitter jouw mening over onze diensten (Max 140 karakters)', background='#ffc400', fg='blue', height='6', font=('Insaniburger', 24))
label.pack(fill=X)


#entries
entry = Entry(master=root, width=60, fg='blue', font=('Bahnschrift', 20))
entry.pack(padx=10, pady=10)


#buttons
button1 = Button(master=root, text='Verzenden', command=clicked, font=('Insaniburger', 10), bg='blue', fg='#ffc400')
button1.pack(pady=10)

button2 = Button(root, height=4, bd=0, bg='blue', fg='#ffc400', text="Exit", font=('Insaniburger', 20), command=root.destroy).pack(side='bottom', fill=X)

root.mainloop()